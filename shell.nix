{ nixpkgs ? import ./nix/pinned-nixpkgs.nix
, compiler ? "default"
}:

let
  inherit (nixpkgs) pkgs;
  env = (import ./default.nix { inherit nixpkgs compiler; }).env;
in
  env.overrideAttrs (oldAttrs: {
    buildInputs = with pkgs.haskellPackages; oldAttrs.buildInputs ++ [
      cabal-install ghcid
    ];
  })
