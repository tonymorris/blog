---
comments: true
date: 2019-06-13
layout: post
slug: vans-aircraft-factory
title: Van's Aircraft Factory visit 2019-06-10
tags: Aviation
---

On 10 June 2019, I visited the [Van's Aircraft Factory in Aurora, Oregon](https://www.openstreetmap.org/?mlat=45.2423&mlon=-122.7658#map=15/45.2423/-122.7658). The week prior, I had been in Boulder, Colorado, for [Lambdaconf](http://lambdaconf.us/), which is one of the most amazing and diverse functional programming conferences in the world. The idea was to test-fly the [Vans RV-14 aircraft](https://www.vansaircraft.com/rv-14/), ownership of which has been a dream for quite some time. The specifications, performance and price of the RV-14 has always amazed me.

Here are some specifications of the RV-14:

+:-------------------------------+:-----------------------------:+
| **Airframe**                   | 2-seats, aerobatic, low-wing  |
+--------------------------------+-------------------------------+
| **Engine**                     | Lycoming IO-390 210hp         |
+--------------------------------+-------------------------------+
| **MTOW**                       | 930kg                         |
+--------------------------------+-------------------------------+
| **Fuel capacity**              | 190L                          |
+--------------------------------+-------------------------------+
| **Usable load**                | 368kg                         |
+--------------------------------+-------------------------------+
| **Usable load with full fuel** | 231kg                         |
+--------------------------------+-------------------------------+
| **Baggage**                    | 45.3kg                        |
+--------------------------------+-------------------------------+
| **Take-off distance**          | 512m                          |
+--------------------------------+-------------------------------+
| **Landing distance**           | 550m                          |
+--------------------------------+-------------------------------+
| **Rate of climb**              | 9.1m/s                        |
+--------------------------------+-------------------------------+
| **Cruise speed**               | 176kt                         |
+--------------------------------+-------------------------------+
| **Vne speed**                  | 200kt                         |
+--------------------------------+-------------------------------+

----

The RV-14 also includes a [Constant Speed Propellor](https://en.wikipedia.org/wiki/Constant-speed_propeller). In Australia, this (legally) requires a *feature endorsement* on your pilot licence to fly an aeroplane with this feature. Australia's regulator calls the endorsement, [Multi-Pitch Propellor Control (MPPC)](https://www.casa.gov.au/standard-page/aircraft-design-feature-endorsements).

The week before Lambdaconf, I went to [Flightscope Aviation](http://flightscopeaviation.com.au/) with the goal of achieving a MPPC endorsement. The instructors at Flightscope have always been helpful, educational and fun to fly with. I did the MPPC endorsement in the *beautiful* [Aquila A210](https://www.flightscopeaviation.com.au/fleet).

I arrived early at Vans Aircraft Factory, still with some disbelief that I was actually there!

<a href="/images/vans-aircraft-factory/IMG_20190610_085646-zoomed.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_085646-zoomed.jpg" alt="turn-off to Vans Aircraft Factory" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_090255.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_090255_23.9pc.jpg" alt="Front of Vans Aircraft Factory" style="width: 1110px;"/></a>

----

I chatted to a couple of the people in the lobby. A pair of them had flown into Aurora State Airport (KUAO) in a [Vans RV-9](https://www.vansaircraft.com/rv-9/). The lobby has a display of the different types of avionics that can be fitted to Vans aircraft; most of them from [Dynon](https://www.dynonavionics.com/). The other person in the lobby introduced himself and told me that he was keen to fly the RV-14.

Not long after, we were taken into the aircraft factory, where there were rows and rows of different aircraft parts. We walked passed a crate being loaded with a RV-14 fuselage, for delivery to some happy customer.

----

<a href="/images/vans-aircraft-factory/IMG_20190610_100255.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100255_23.9pc.jpg" alt="Vans Aircraft Factory" style="width: 1110px;"/></a>

----

We were shown the sheet metal cutting and bending machines.

----

<a href="/images/vans-aircraft-factory/IMG_20190610_100306.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100306_23.9pc.jpg" alt="Vans Aircraft Factory" style="width: 1110px;"/></a>

----

<iframe width="1110" height="624" src="https://www.youtube.com/embed/klkAv74iK94" frameborder="0" allowfullscreen></iframe>

----

<iframe width="1110" height="624" src="https://www.youtube.com/embed/94dRMq9e_IA" frameborder="0" allowfullscreen></iframe>

It wasn't long before we were in the hangar. And there it was, in front of my eyes, the RV-14. I was in a little shock, but the guide was still casually chatting away with one of the other hopeful flyers.

<a href="/images/vans-aircraft-factory/IMG_20190610_103409.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103409_23.9pc.jpg" alt="Vans RV-14 and Vans RV-12iS" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_102909.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_102909_23.9pc.jpg" alt="Vans RV-14" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_100824.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100824_23.9pc.jpg" alt="Vans RV-14" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_100854.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100854_23.9pc.jpg" alt="Vans RV-14" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_102859.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_102859_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

The RV-14 is the conventional gear version of the aircraft, while the RV-14/A is the tricycle gear (nose-wheel). "The tricycle gear is out on a training mission", we were told. I've flown a tail-wheel aircraft before. I was just amazed. Next to the RV-14 was a RV-12iS, the light-sport aircraft from Vans, with a 100hp Rotax 912iS engine. This is an aircraft category that we also have in Australia, limited to two seats and a MTOW of 600kg.

<a href="/images/vans-aircraft-factory/IMG_20190610_101502.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101502_23.9pc.jpg" alt="Vans RV-12iS" style="width: 1110px;"/></a>

----

Also in the same hangar were a RV-8/A and RV-9/A.

<a href="/images/vans-aircraft-factory/IMG_20190610_101448.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101448_23.9pc.jpg" alt="Vans RV-8/A and Vans RV-9/A" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_103428.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103428_23.9pc.jpg" alt="Vans RV-8/A and Vans RV-9/A" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_103526.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103526_23.9pc.jpg" alt="Vans RV-8/A and Vans RV-9/A" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_100518.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100518_23.9pc.jpg" alt="Vans RV-8/A and Vans RV-9/A" style="width: 1110px;"/></a>

----

First thing to do was to sit in the left seat. The seat was adjusted at the "medium" setting, and I still could barely reach the rudder pedals. "There's a lot of room in here."

I admired the avionics and neat cockpit organisation. The control stick having elevator trim, aileron trim and even flap setting on the right side. I was still coming to terms with, "I am going to fly this!"

<a href="/images/vans-aircraft-factory/IMG_20190610_101327.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101327_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

Soon, enough, we were pushing the aircraft out of the hangar where the engine could be started.

<a href="/images/vans-aircraft-factory/IMG_20190610_105100.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_105100_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

KUAO is a class D controlled airport, similar to the airport that I usually fly out of (YBAF). We called up the tower and asked for taxi clearance. The forward visibility in conventional gear aircraft is renowned for its difficulty. I remembered the Vans RV-7 that hit a concrete weather post with its propeller back home, while I was doing my training. Woops! We taxied the RV-14 to the threshold of runway 35 to do our run-ups. The 6.4 litre, IO-390 quickly spun up to 1700rpm while we checked the dual ignition system, flight controls, etc.

During the run-up, I heard tower clear an aircraft to land, "November two one four victor alpha, runway 35, cleared to land." I knew that registration; it was the other RV-14, the tricycle gear. I watched it coming in on short final.

We called up the tower and announced that we were ready. We quickly received our take-off and off we went!

<a href="/images/vans-aircraft-factory/IMG_20190610_110744.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_110744_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/IMG_20190610_110749.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_110749_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

We climbed out at 74KIAS (a little faster than Vx) with an indicated climb performance of 1600fpm! That was an amazing number to see. Nothing I'd flown before performs like that. Well, except for the [Yakovlev Yak-52](https://en.wikipedia.org/wiki/Yakovlev_Yak-52) while in the back seat, but that panel was in Russian, so I had no idea what the climb performance was.

During the climb out, [Mt Adams *(12281 ft)*](https://en.wikipedia.org/wiki/Mount_Adams_(Washington)) and [Mt St Helens *(8363ft)*](https://en.wikipedia.org/wiki/Mount_St._Helens) could be seen out the right window. Mt St Helens, renowned for the 1980 eruption in which 57 people were killed. I could see it with my own eyes. Keep in mind, the tallest mountain in Australia is a little over 7000ft.

<a href="/images/vans-aircraft-factory/IMG_20190610_110737.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_110737_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

We got clear of the local airport and I did some 60 degree AoB (2G) steep turns. This thing is fast! Here is a diagram of our flight track:

<a href="/images/vans-aircraft-factory/flight-track-1.png"><img src="/images/vans-aircraft-factory/flight-track-1.png" alt="" style="width: 1110px;"/></a>

----

<a href="/images/vans-aircraft-factory/flight-track-2.png"><img src="/images/vans-aircraft-factory/flight-track-2.png" alt="" style="width: 1110px;"/></a>

----

Next it was time for some stalls. This involves reducing engine power to idle, while maintaining level flight by gradually pitching the aircraft up. Eventually, the aircraft will aerodynamically stall and stop flying. Different aircraft have various characteristics when they stall; I had no idea what the RV-14 would do, though the recovery procedure should always be the same. I had never experienced such a gentle stall in an aircraft of this weight. It was a gentle stall, with no dropping of the wing, and a trivial recovery by releasing the control stick (no pitch down input required!).

I decided to perform a more aggressive stall. This means that as the aircraft slows down and approaches the moment of stall, apply a sudden pitch up input on the control stick to bring on the stall sharply. Typically, this involves losing sight of the horizon as the aircraft pitches up. Again, it was a gentle stall as the nose of the aircraft pointed to the ground, again without any wing drop.

Let's try it again in approach configuration, with flaps. This typically means one of the wings will drop when it stalls and this time it did, but *only just*. After recovering to normal level flight, I started wondering how a high-performance aircraft can stall so gently. The aerofoil is a constant-length chord, but with a Vne of 200 knots, I was expecting something a little more slippery and dramatic in a stall!

We did some slow flying and handling, before beginning a slow descent back to the airport. Soon enough, we were on a left base for runway 35.

<a href="/images/vans-aircraft-factory/IMG_20190610_112318.jpg.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_112319_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

Mt Hood, Mt Adams and Mt St Helens can be seen to the east. Time to land!

<iframe width="1110" height="624" src="https://www.youtube.com/embed/3rqhdvpxr4c" frameborder="0" allowfullscreen></iframe>

----

Here is a 3D visualisation of the flight:

<iframe src="https://ayvri.com/embed/095mxd00jz/cjwr1uug000011w5w7h93sxt4" allowfullscreen height="833" width="1110"></iframe>

----

<iframe width="1110" height="624" src="https://www.youtube.com/embed/D_meReBqVxM" frameborder="0" allowfullscreen></iframe>

----

Once on the ground, I chilled out airside and admired some of the other aircraft. A Vans RV-9/A and two RV-10s.

<a href="/images/vans-aircraft-factory/IMG_20190610_114729.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_114729_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

The RV-10 is the (only) four-seat aircraft from Vans, equipped with a Lycoming IO-540 at 260hp. This thing *flies*.

<a href="/images/vans-aircraft-factory/IMG_20190610_100802.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100802_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

<iframe width="1110" height="624" src="https://www.youtube.com/embed/DNMWdSpXd-M" frameborder="0" allowfullscreen></iframe>

----

<iframe width="1110" height="624" src="https://www.youtube.com/embed/0_Xx5q5Cv8I" frameborder="0" allowfullscreen></iframe>

----

One more piccie of the RV-14 cockpit.

<a href="/images/vans-aircraft-factory/IMG_20190610_113135.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_113135_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

I saw a bunch of people having a talk across the parking area.

<a href="/images/vans-aircraft-factory/IMG_20190610_113139.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_113139_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

----

I joined them where I met Richard VanGrunsven and Mike Seager. The discussion was about incipient spin training during the initial stages of learning. I told them that in Australia, incipient spin and recovery is a required demonstrated competency for getting a pilot licence. I was quickly reminded of [VH-MPM :(](https://www.atsb.gov.au/publications/investigation_reports/2017/aair/ao-2017-096/), the report which notes that there is no consensus on the meaning of an incipient spin.

I chilled out a bit longer on the airfield before I decided to head back. I couldn't stop thinking about how I could possibly ever own and fly a Vans RV-14. One day.

One the way out, a (very noisy) Sikorsky CH-54A was landing after "skycrane" operations (I'd heard them on the radio earlier).

<iframe width="1110" height="624" src="https://www.youtube.com/embed/6t3MZzz-ISA" frameborder="0" allowfullscreen></iframe>

Thanks Vans, it was a blast!

<!--
<a href="/images/vans-aircraft-factory/IMG_20190610_103816.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103816_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_100253.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100253_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_105102.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_105102_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_103536.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103536_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_113117.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_113117_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_101719.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101719_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_100956.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100956_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_103414.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103414_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_102914.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_102914_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_124750.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_124750_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_110739.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_110739_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_105101.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_105101_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_123150.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_123150_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_085646.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_085646_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_105050.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_105050_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_101454.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101454_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_120300.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_120300_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_103824.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103824_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_112321.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_112321_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_110741.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_110741_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_123256.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_123256_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_103412.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103412_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_123253.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_123253_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_100257.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_100257_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_112318.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_112318_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_105057.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_105057_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_123419.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_123419_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_090217.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_090217_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_103530.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_103530_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_101500.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101500_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_101327.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101327_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_101728.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_101728_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<a href="/images/vans-aircraft-factory/IMG_20190610_123250.jpg"><img src="/images/vans-aircraft-factory/IMG_20190610_123250_23.9pc.jpg" alt="" style="width: 1110px;"/></a>

<iframe width="1110" height="624" src="https://www.youtube.com/embed/N0iQnVhwLnE" frameborder="0" allowfullscreen></iframe>

<iframe width="1110" height="624" src="https://www.youtube.com/embed/3csW955ObYc" frameborder="0" allowfullscreen></iframe>

RWY35 KUAO
1730 29:43 N144VA taxi
1730 30:44 N214VA taxi to Vans
1800 00:00 N144VA taxi via A
1800 00:09 N317VA traffic notify
1800 05:22 Cherokee landing (N144VA just finished run-up)
1800 05:51 N144VA take-off clearance
1800 21:56 N144VA cleared to land

-->
