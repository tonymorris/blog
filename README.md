# blog.tmorris.net

### Build and run locally

##### cabal/nix

* `nix-shell --run 'cabal new-run site -- build'`
* `nix-shell --run 'cabal new-run site -- watch'`

##### stack

* `stack install --only-dependencies`
* `export STACK_ROOT=$(pwd)/.stack` 
* `stack setup`
* `stack build`
* `stack exec site watch`
